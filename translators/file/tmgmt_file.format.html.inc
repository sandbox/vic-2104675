<?php

/**
 * Export into HTML.
 */
class ExtendTMGMTFileFormatHTML extends TMGMTFileFormatHTML 
{
  /**
   * Implements TMGMTFileExportInterface::import().
   */
  public function import($imported_file) 
  {
    $dom = new DOMDocument();
    $dom->loadHTMLFile($imported_file);
    $xml = simplexml_import_dom($dom);

    $data = array();
    foreach ($xml->xpath("//div[@class='atom']") as $atom) {
      // Assets are our strings (eq fields in nodes).
      $key = $this->decodeIdSafeBase64((string) $atom['id']);
      
      if ($atom->children()->count()) {
        $text = array();
        foreach ($atom->children() as $child) {
          $text[] = $child->asXML();
        }
        $data[$key]['#text'] = implode('', $text);
      } else {
        $data[$key]['#text'] = (string) $atom;
      }
    }
    return tmgmt_unflatten_data($data);
  }
}
