<?php

/**
 * Implements hook_tmgmt_source_translation_structure().
 * This hook is implemented on behalf of the core text module.
 */
function link_tmgmt_source_translation_structure($entity_type, $entity, $field, $instance, $langcode, $items) 
{
  $structure = array();
  if (!empty($items)) {
    $structure['#label'] = check_plain($instance['label']);
    foreach ($items as $delta => $value) {
      foreach ($value as $column_name => $column_value) {
        if ($column_name == 'title') {
          $structure[$delta]['#label'] = t('Delta #@delta', array('@delta' => $delta));
          $structure[$delta][$column_name] = array(
            '#label' => $structure['#label'],
            '#text' => $column_value,
            '#translate' => TRUE,
          );
        } else {
          $structure[$delta][$column_name] = array(
            '#label' => '',
            '#text' => $column_value,
            '#translate' => FALSE,
          );
        }
      }
    }
  }
  return $structure;
}
